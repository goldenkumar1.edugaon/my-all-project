package adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.tablayout.Tab1
import com.example.tablayout.Tab2
import com.example.tablayout.Tab3

class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(poasition: Int): CharSequence? {
        when (poasition) {
            0 -> {
                return "Tab1"
            }
            1 -> {
                return " Tab2 "
            }

            2 -> {
                return " Tab3 "
            }

        }
        return super.getPageTitle(poasition)
    }

    override fun getItem(poasition: Int): Fragment {

        return when (poasition) {
            0 -> {
                Tab1()
            }

            1 -> {
                Tab2()
            }

            else -> {
                Tab3()
            }

        }

    }
}

