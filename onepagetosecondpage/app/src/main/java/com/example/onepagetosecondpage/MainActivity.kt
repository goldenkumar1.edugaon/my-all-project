package com.example.onepagetosecondpage

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    val name =  findViewById<EditText>(R.id.name)
    val email =  findViewById<EditText>(R.id.email)
    val password = findViewById<EditText>(R.id.passwor)
    val button = findViewById<Button>(R.id.button)

       button.setOnClickListener {
       val intent = Intent(this,profileactivity::class.java)

       intent.putExtra("name",name.text.toString())
       intent.putExtra("email",email.text.toString())
       intent.putExtra("password",password.text.toString())
           startActivity(intent)

       }

    }
}