package com.example.onepagetosecondpage

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView

class profileactivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profileactivity)

    val profile =findViewById<TextView>(R.id.profile)

    val name = intent.extras?.getString("name","")
    val email = intent.extras?.getString("email","")
    val password = intent.extras?.getString("password","")

    profile.text=name.toString()+"\n"+email.toString()+"\n n"+password.toString()
    }

}